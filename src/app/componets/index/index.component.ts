import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/SERVICE/product.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  list:any=[];
  public page: number=0;
  filterProduct='';
  // @ts-ignore
  public filterPrice: number=undefined;
  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.listAll();
  }

  delete(id: string){
    this.productService.deleteProduct(id).subscribe(
      res=>{this.ngOnInit();},
      err=>console.log(err)
    );
  }

  listAll(){
    this.productService.getListAll().subscribe(
      res=>{this.list=res},
      err=>console.log(err)
    );
  }
}
