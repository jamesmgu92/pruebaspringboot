import { Component, OnInit } from '@angular/core';
import { Product, ProductService } from 'src/app/SERVICE/product.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  productNew: Product={id:'',creation_date: new Date(), description:'',  expiry_date:new Date(),  name:'',  price:0};
  constructor(private productService: ProductService, private router: Router ) { }

  ngOnInit(): void {
  }


  createProduct(){

    this.productService.insertProduct(this.productNew).subscribe(
        res=>{
          console.log(res);
          this.router.navigate(['/index']);
        },
        err=>console.log(err)
    )
  }

}
