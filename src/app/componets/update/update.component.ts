import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, ParamMap, Params} from '@angular/router';
import { Product, ProductService } from 'src/app/SERVICE/product.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  // @ts-ignore
  parametros: Subscription=null;
  id: string="";
  productAct: Product={id:'',creation_date: new Date(), description:'',  expiry_date:new Date(),  name:'',  price:0};
  constructor(
    private productService: ProductService,
    private activateRouter: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {

  this.parametros=this.activateRouter.params.subscribe(
    (params: Params)=>{
      this.id=params['id'];
    }
  );
    console.log(this.id);
    this.productService.getSearchId(this.id).subscribe(
      res=>{this.productAct=res},
      err=>console.log(err)
    );
  }

  save(){
    this.productService.editProduct(this.id, this.productAct).subscribe(
      res=>{
        this.router.navigate(['/index']);
      },
      err=>console.log(err)
    );
  }

}
