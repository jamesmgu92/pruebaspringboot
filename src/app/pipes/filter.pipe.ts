import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, arg:any): any {
    if(arg===''|| arg===undefined || arg.length < 2 ) return value;
    const resultProduct = [];
    for(const product of value){
      if(product.name.toLowerCase().indexOf(arg.toLowerCase()) > -1){
        resultProduct.push(product);
      };
    };

    for(const product of value){
      if(product.price <= arg ){
        resultProduct.push(product);
      };
    };
    return resultProduct;
  }

}
