import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  url='http://localhost:8080/index/product';
  constructor(private http: HttpClient) { }

  getListAll():Observable<any>
  {
    return this.http.get(this.url);
  }
  getSearchId(id: string):Observable<any>
  {
    return this.http.get(this.url+'/'+id);
  }

  insertProduct(product: Product):Observable<any>
  {
    return this.http.post(this.url, product);
  }
  editProduct(id: string, product: Product):Observable<any>
  {
    return this.http.put(this.url+'/'+id, product);
  }
  deleteProduct(id: string):Observable<any>
  {
    return this.http.delete(this.url+'/'+id);
  }

}

export interface Product{
  id:string;
  creation_date:Date;
  description:string;
  expiry_date:Date;
  name:string;
  price:number;
}
