import { NgModule } from '@angular/core';
import { RouterModule, Routes , ActivatedRoute} from '@angular/router';
import{ IndexComponent} from './componets/index/index.component'
import{ UpdateComponent} from './componets/update/update.component'
import{ CreateComponent} from './componets/create/create.component'


const routes: Routes = [
  {path: '', redirectTo : 'index', pathMatch:'full'},
  {path: 'index', component: IndexComponent},
  {path: 'update/:id', component: UpdateComponent},
  {path: 'create', component: CreateComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
